const HtmlWebpackPlugin = require('html-webpack-plugin');
const CreateFileWebpack = require('create-file-webpack');
const path = require('path');
const util = require('util');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const SymlinkWebpackPlugin = require('symlink-webpack-plugin');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');

const rootPath = path.resolve(__dirname, "..");
const buildPath = path.join(rootPath, "build");
const assetsPath = path.join(buildPath, "assets");
const devDependencies = require(path.join(rootPath, 'package.json')).devDependencies;
const kernelVersion = devDependencies["@basthon/gui-base"];
let _sys_info;

const languages = {
    "python3": "Python 3",
    "javascript": "JavaScript",
    "sql": "SQL",
    "ocaml": "OCaml"
};

// build sys_info variable
async function sys_info() {
    if (_sys_info != null) return _sys_info;
    const exec = util.promisify(require('child_process').exec);
    let commitHash = await exec('git rev-parse HEAD');
    commitHash = commitHash.stdout.trim();
    let commitDate = await exec('date +%Y/%m/%d_%H:%M:%S');
    commitDate = commitDate.stdout.trim();
    _sys_info  = {
        "kernel-version": kernelVersion,
        "commit-hash": commitHash,
        "commit-date": commitDate,
    };
    return _sys_info;
}

// build version file
async function versionFile() {
    return new CreateFileWebpack({
        content: JSON.stringify(await sys_info(), null, 2),
        fileName: "version",
        path: assetsPath
    });
}

// generate index.html from template src/templates/index.html
async function html(language) {
    const sysInfo = JSON.parse(JSON.stringify(await sys_info()));
    sysInfo['language'] = language;
    sysInfo['language-name'] = languages[language];
    return new HtmlWebpackPlugin({
        hash: true,
        sys_info_js: JSON.stringify(sysInfo),
        sys_info: sysInfo,
        template: "./src/templates/index.html",
        filename: `../${language}/index.html`,
        publicPath: "assets/",
    });
}

// all index.html
async function htmls() {
    const result = [];
    for(const language of Object.keys(languages))
        result.push(await html(language));
    return result;
}

// bundle css
function css() {
    return new MiniCssExtractPlugin({
        filename: "[name].css"
    });
}

function copies() {
    return new CopyPlugin({
        patterns: [
            { // htaccess
                from: "./src/.htaccess", to: buildPath
            },
            { // python3 files
                from: "**/*",
                context: "./node_modules/@basthon/kernel-python3/lib/dist/",
                to: path.join(assetsPath, kernelVersion),
                toType: "dir"
            },
            { // examples
                from: "examples/**/*",
                to: buildPath,
                toType: "dir"
            },
        ]
    });
}

function languageSymlinks() {
    const links = [
        { origin: '../python3/index.html', symlink: '../index.html', force: true },
        { origin: '../python3/', symlink: '../python', force: true },
        { origin: '../javascript/', symlink: '../js', force: true }
    ];
    Object.keys(languages).forEach(language =>
        ['assets', 'examples'].forEach(folder =>
            links.push( { origin: `../${folder}/`,
                          symlink: `../${language}/${folder}`,
                          force: true } )
        )
    );
    return new SymlinkWebpackPlugin(links);
}

async function main() {
    return {
        entry:  { 'main': "./src/ts/main.ts",
            'editor.worker': 'monaco-editor/esm/vs/editor/editor.worker.js',
            'json.worker': 'monaco-editor/esm/vs/language/json/json.worker',
            'css.worker': 'monaco-editor/esm/vs/language/css/css.worker',
            'html.worker': 'monaco-editor/esm/vs/language/html/html.worker',
            'ts.worker': 'monaco-editor/esm/vs/language/typescript/ts.worker'
        },
        output: {
            filename: '[name].[contenthash].js',
            chunkFilename: '[name].[contenthash].js',
            path: assetsPath,
            clean: true
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, "css-loader"],
                },
                {
                    test: /\.(png|svg|jpg|jpeg|gif)$/i,
                    type: 'asset/resource',
                },
                { // specific rule for kernel-sql
                    resourceQuery: /asset-url/,
                    type: 'asset/resource',
                },
                {
                test: /\.(scss)$/,
                use: [{
                    // inject CSS to page
                    loader: MiniCssExtractPlugin.loader
                    }, 
                    {
                    // translates CSS into CommonJS modules
                    loader: 'css-loader'
                    },
                    {
                        // Run postcss actions
                        loader: 'postcss-loader',
                        options: {
                        // `postcssOptions` is needed for postcss 8.x;
                        // if you use postcss 7.x skip the key
                        postcssOptions: {
                            // postcss plugins, can be exported to postcss.config.js
                            plugins: function () {
                            return [
                                require('autoprefixer')
                            ];
                            }
                        }
                    }
                    },
                    {
                        // compiles Sass to CSS
                        loader: 'sass-loader'
                    }
                ],
            }
            ]
        },
        resolve: {
            extensions: ['.ts', '.js','.scss'],
            fallback: {  // for ocaml bundle
                "constants": require.resolve("constants-browserify"),
                "tty": require.resolve("tty-browserify"),
                "fs": false,
                "child_process": false,
                // for sql bundle
                "crypto": require.resolve("crypto-browserify"),
                "path": require.resolve("path-browserify"),
                "buffer": require.resolve("buffer/"),
                "stream": require.resolve("stream-browserify"),
            },
        },
        plugins: [
            ...await htmls(),
            css(),
            await versionFile(),
            copies(),
            languageSymlinks()
        ],
        devServer: {
            static: {
                directory: buildPath,
            },
            compress: true,
            port: 8888,
        },
    };
}

module.exports = main;
