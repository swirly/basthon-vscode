import GUI from "./gui";
import * as monaco from 'monaco-editor';


export default class Editor {
    private _editor: monaco.editor.IStandaloneCodeEditor;
    private _gui: GUI;

    public constructor(gui: GUI) { 
        this._gui = gui; 
        // monaco mode is python, not python3 
        const language = this._gui.language;
        const editor_language = language !== 'python3' ? language : 'python';
        // Create monaco editor
        let monacoElt = document.getElementById('editor') as HTMLElement;
        this._editor = monaco.editor.create(monacoElt, {
            language: editor_language,
            automaticLayout: true,
            theme:'vs-dark'
        });
    }

    /**
     * Kernel getter.
     */
    public get kernel() { return this._gui.kernel; }
    public get kernelSafe() { return this._gui.kernelSafe; }

    /**
     * Initialize the Ace editor.
     */
    public async init() {
        this._editor.focus();
    }

    /**
     * Set editor's content
     */
    public setContent(content?: string) {
        if (content != null) this._editor.setValue(content);
        this._editor.setPosition({column:0,lineNumber:0});
        this._editor.setScrollLeft(0);
        this._editor.setScrollTop(0);
    }

    /**
     * Get editor's content
     */
    public getContent() {
        return this._editor.getValue();
    }
}
