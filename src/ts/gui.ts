// notie
import * as notie from "notie";
import "notie/dist/notie.css";
// basthon
import { GUIBase, GUIOptions } from "@basthon/gui-base";
import { Storage } from "@basthon/checkpoints";
// bootstrap
import * as bootstrap from 'bootstrap';
import '../css/main.scss';
import 'bootstrap-icons/font/bootstrap-icons.css';
// local
import Shell from "./shell";
import Editor from "./editor";
import Graphics from "./graphics";


export default class GUI extends GUIBase {
    private _stateStorage = new Storage("console.states");
    private _states: { [key: string]: boolean; } = {};
    private _shell: Shell;
    private _editor: Editor;
    private _graphics: Graphics;
    private _has_shell: boolean = true;
    private _has_graphics: boolean = true;

    public constructor(options: GUIOptions) {
        super((() => { options.uiName = "console"; return options; })());
        this._urlKey = "script";
        this._contentFilename = "script";
        this._shell = new Shell(this);
        this._editor = new Editor(this);
        this._graphics = new Graphics(this);
        /* when kernel is available, set contentFilename extension */
        this.kernelLoader.kernelAvailable().then((kernel) => {
            const exts = kernel.moduleExts();
            if (exts.length) this._contentFilename += '.' + exts[0];
        });
    }

    /**
     * Get editor's content.
     */
    public content(): string { return this._editor.getContent(); }

    /**
     * Set editor's content.
     */
    public setContent(content: string): void { this._editor.setContent(content); }

    /**
     * Editor getter.
     */
    public get editor() { return this._editor; };

    /**
     * Shell getter.
     */
    public get shell() { return this._shell; };

    /**
     * Graphics getter.
     */
    public get graphics() { return this._graphics; };

    
    /**
     * Print error.
     */
    public error(title: string, message:string): boolean {
        let alertDiv = document.createElement("div");
        alertDiv.classList.add("alert", "alert-danger", "alert-dismissible", "fade", "show");
        let content = "<strong>"+ this.escapeHtml(title) +"</strong>\n";
        let regexp = /<br>/gi;
        let contenu =  this.escapeHtml(message.replace(regexp,'\n'));
        content += "<pre>\n" + contenu + "</pre>";
        content += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        alertDiv.innerHTML = content;

        let container = document.querySelector("#basthonVScontainer");
        container?.prepend(alertDiv);

        let tab = document.getElementById("editor-tab") as HTMLElement;
        let bstab = new bootstrap.Tab(tab);
        bstab.show();

        return true;
    }; 

    //    public error(title: string, message: string) {
        
    //     notie.alert({
    //         type: 'error',
    //         text: message,
    //         stay: false,
    //         time: 3,
    //         position: 'top',
    //     });
    // }
    
     /**
     * Notify the user.
     */
      public info(title: string, message: string) {
        notie.alert({
            type: 'success',
            text: message,
            stay: false,
            time: 3,
            position: 'top'
        });
    }


    /**
     * Ask the user to confirm or cancel.
     */
     public confirm(
        title: string,
        message: string,
        text: string,
        callback: (() => void),
        textCancel: string,
        callbackCancel: (() => void)): void {
        notie.confirm({
            text: message,
            submitText: text,
            cancelText: textCancel,
            position: 'top',
            submitCallback: callback,
            cancelCallback: callbackCancel
        });
    }

    /**
     * Ask the user to select a choice.
     */
    public select(
        title: string,
        message: string,
        choices: {
            text: string,
            handler: () => void
        }[],
        textCancel: string,
        callbackCancel: (() => void)) {
        let count = 0;
        notie.select({
            text: message,
            cancelText: textCancel,
            position: 'top',
            choices: choices.map(
                (c) => ({
                    text: c.text,
                    type: (count++ % 2) ? 'error' : 'success',
                    handler: c.handler || (() => { }),
                })),
            cancelCallback: callbackCancel || (() => { }),
        });
    }
    protected async setupUI(options: any) {
        await super.setupUI(options);
        /* instanciate the storage */
        await this._stateStorage.ready();

        await Promise.all([
            this._editor.init(),
            this._shell.init(),
            this._graphics.init()
        ]);

        /* Initialize Bootstrap tooltips */
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        tooltipTriggerList.map(function (tooltipTriggerEl) {
          return new bootstrap.Tooltip(tooltipTriggerEl)
        })


        /* connecting buttons */
        // run
        let button = document.getElementById('run');
        button?.addEventListener('click', () => this.runScript());
        // open
        button = document.getElementById("open");
        button?.addEventListener("click", () => this.openFile());
        // download
        button = document.getElementById("download");
        button?.addEventListener("click", () => this.download());
        // share
        button = document.getElementById("share");
        button?.addEventListener("click", () => this.share());

        // raz
        button = document.getElementById('raz');
        button?.addEventListener('click', () => this.restart());


        /* binding ctrl+s */
        window.addEventListener("keydown", (event) => {
            if ((event.ctrlKey || event.metaKey) &&
                String.fromCharCode(event.which).toLowerCase() === "s") {
                event.preventDefault();
                this.download();
            }
        });

        /* backup before closing */
        window.onbeforeunload = () => { this.backup().catch(this.notifyError.bind(this)); };
    }

    /**
     * Init a state variable from storage (with default value).
     */
    public async initStateFromStorage(state: string, _default: boolean = false) {
        this._states[state] = (await this._stateStorage.get(state)) as boolean;
        if (this._states[state] == null) this._states[state] = _default;
    }

    /**
     * Save a state variable in storage.
     */
    public async saveStateInStorage(state: string) {
        await this._stateStorage.set(state, this._states[state]);
    }

    /**
     * Run the editor script in the shell.
     */
    public async runScript() {
        await this.loaded();
        const src = this.content();
        if (this._has_shell) {
            let tabElement = document.getElementById("shell-tab") as HTMLElement;
            let tab = new bootstrap.Tab(tabElement);
            tab.show();
        } else if (this._has_graphics) {
            let tabElement = document.getElementById("graphics-tab") as HTMLElement;
            let tab = new bootstrap.Tab(tabElement);
            tab.show();
        }
        this.restart();
        this._shell.launch(src, false);
    }

    /**
     * RAZ function.
     */
    public restart() {
        this.kernelRestart();
        this._shell.clear();
        this._graphics.clean();
    }

    /**
     * Open *.py file by asking user what to do:
     * load in editor or put on (emulated) local filesystem.
     */
    public async openModuleFile(file: File) {
        this.confirm("", `Que faire de ${file.name} ?`,
            "Charger dans l'éditeur",
            () => { this.open(file); },
            "Installer le module",
            () => { this.putFSRessource(file); }
        );
    }

    /**
     * Opening file: If it has kernel extension, loading it in the editor
     * or put on (emulated) local filesystem (user is asked to),
     * otherwise, loading it in the local filesystem.
     */
    public async openFile() {
        await this.kernelLoader.kernelAvailable();
        const callback = this.openModuleFile.bind(this);
        const callbacks: { [key: string]: ((_: File) => Promise<void>) } = {};
        this.kernelSafe?.moduleExts().forEach((ext: string) => {
            callbacks[ext] = callback
        });
        return await this._openFile(callbacks);
    }

    public escapeHtml(unsafe:string): string{
        return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
        }
}
