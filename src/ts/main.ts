import GUI from "./gui";

declare global {
    interface Window {
        basthonLanguage: string;
        basthonRoot: string;
    }
}

const gui = new GUI({
    "kernelRootPath": window.basthonRoot,
    "language": window.basthonLanguage
});

gui.init();
