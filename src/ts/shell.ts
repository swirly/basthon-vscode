import GUI from "./gui";
// bootstrap
import * as bootstrap from 'bootstrap';

export default class Shell {
    private _elem = document.getElementById("shell") as HTMLElement;
    private _gui: GUI;

    public constructor(gui: GUI) { this._gui = gui; }

    /**
     * Kernel getter.
     */
    public get kernel() { return this._gui.kernel; }
    public get kernelSafe() { return this._gui.kernelSafe; }

    /**
     * Getter for DOM element.
     */
    public get domElement() { return this._elem; }

    /**
     * Initialize the shell.
     */
    public async init() {
        // just to be safe
        this.clear();
        await this._gui.kernelLoader.kernelAvailable();

        /* Event connections */
        this._gui.kernelLoader.kernelLoaded().then(() => {
            this.kernel?.addEventListener("eval.finished", (data) => {
                if (data.interactive && "result" in data) {
                    this.echo(data.result["text/plain"] + '\n');
                }
            });

            this.kernel?.addEventListener("eval.output", (data) => {
                switch (data.stream) {
                    case "stdout":
                        this.echo(data.content);
                        break;
                    case "stderr":
                        this.error(data.content);
                        break;
                }
            });
        });
    }

    /**
     * Print a string in the shell.
     */
    public echo(message: string) {
        message = message.toString();
        let element = document.querySelector('#shell .shellInterior') as HTMLElement;
        element.innerHTML += this._gui.escapeHtml(message);
        return true;
    };

    /**
     * Print error in shell (in red).
     */
    public error(message:string): boolean {
        let alertDiv = document.createElement("div");
        alertDiv.classList.add("alert", "alert-danger", "alert-dismissible", "fade", "show");
        let content = "<strong>Erreur Python</strong>\n";
        content += "<pre>\n" + this._gui.escapeHtml(message) + "</pre>";
        content += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        alertDiv.innerHTML = content;

        let container = document.querySelector("#basthonVScontainer");
        container?.prepend(alertDiv);

        let tab = document.getElementById("editor-tab") as HTMLElement;
        let bstab = new bootstrap.Tab(tab);
        bstab.show();

        return true;
    };

    /**
     * Recover the shell as it was at start.
     */
    public clear() {
        this._elem.innerHTML = '<pre class="shellInterior"></pre>';
    };

    /**
     * Launch code in shell.
     */
    public launch(code: string, interactive: boolean = true) {
        if (this.kernelSafe == null) return;
        this.kernelSafe.dispatchEvent("eval.request", {
            code: code,
            interactive: interactive
        });
    };
}
